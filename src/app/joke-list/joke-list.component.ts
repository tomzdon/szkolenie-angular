import { Component, OnInit } from '@angular/core';
import { Joke } from 'src/app/joke.model';
import { ViewChild } from '@angular/core';
import { JokeComponent } from 'src/app/joke/joke.component';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['./joke-list.component.css']
})
export class JokeListComponent implements OnInit {
  jokes: Joke[];
  @ViewChild(JokeComponent) jokeViewChild: JokeComponent;

  constructor() {
    this.jokes = [
      new Joke("What did the cheese say when it looked in the mirror?", "Hello-me (Halloumi)"),
      new Joke("What kind of cheese do you use to disguise a small horse?", "Mask-a-pony (Mascarpone)"),
      new Joke("A kid threw a lump of cheddar at me", "I thought ‘That’s not very mature’"),
    ];
  }

  ngOnInit() {
    console.log("Tuuuutaj   !" ,(this.jokeViewChild))
  }
  ngAfterViewInit() {
    console.log("Tuuuutaj   !" ,(this.jokeViewChild))

  }

  addJoke(joke) {
    this.jokes.unshift(joke);
  }
  removeJoke(joke) {
    
    this.jokes = this.jokes.filter(e=>e !== joke)
  }

}
