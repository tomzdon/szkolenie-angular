import { Component, OnInit, Input } from '@angular/core';
import { Joke } from 'src/app/joke.model';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class JokeComponent implements OnInit {
  @Input() joke : Joke;
  @Output() jokeDelete = new EventEmitter<Object>();
  constructor() {
    console.log(`new - data is ${this.joke}`);
  }

  ngOnChanges() {
    console.log(`ngOnChanges - data is ${this.joke}`);
  }

  ngOnInit() {
    console.log(`ngOnInit  - data is ${this.joke}`);
  }

  ngDoCheck() {
    console.log("ngDoCheck")
  }

  ngAfterContentInit() {
    console.log("ngAfterContentInit");
  }

  ngAfterContentChecked() {
    console.log("ngAfterContentChecked");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy");
  }

  deleteJoke(joke){
    this.jokeDelete.emit(joke);
  }
}
